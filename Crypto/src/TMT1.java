import FormatIO.Console;
import FormatIO.FileOut;

import java.util.Random;

public class TMT1 {
	private static Console	con;
	private static String	file_out;
	private static  int 	cipherInt, plainInt;
	private static Random 	rand	= new Random();
	
	/**
	 * This function generates chain for TMT table. It takes in plainTxt and output file name
	 * as input and the generated (X0,plainTxt) combination is exported into output file
	 * @param plainTxt
	 * @param file_out
	 */
	public static void generateTMTTable(String plainTxt, String file_out){

		plainInt  = Hex16.convert(plainTxt);
		
		FileOut fout = new FileOut(file_out);
		
		for( int i=0; i<Math.pow(2,16); i++) {
			int X0		= rand.nextInt((int) Math.pow(2, 16));
			cipherInt	= X0;
			for( int j=0; j<Math.pow(2, 8); j++ ) {
				cipherInt 	= Coder.encrypt(cipherInt, plainInt);
			}
			fout.println(cipherInt+" "+X0);
		}
		fout.close();
	}

	public static void main(String[] args) {
		// Create a Console for IO
		con = new Console("TMT1");
		
		con.print("Enter Plain Text:");
		String plainTxt  = con.readWord();
		
		con.print("Enter Output File Name: ");
		file_out = con.readWord()+".txt";
		
		generateTMTTable(plainTxt,file_out);

		con.println("-- Finished Caculating TMT Values--");
	}
}
