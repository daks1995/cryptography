import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import FormatIO.Console;

public class CTO {
	/*
	 * This program calculates key and decodes message for Cipher-Text Only Attack (CTO)
	 * based on Brute Force method. This program takes in
	 * know only cipher text and top 100 English words
	 * and runs Brute Force algorithm to check for each key if the de-crypted
	 * message matches and makes sense in English. Each key is ranked based on this
	 * and the key with the maximum rank value is the key to the message. 
	 */

	private static Console	con;
	private static int 		cipherLen;
	private static String 	cipherKey = "";
	private static String 	cipherTxt = "";
	private static String[] cipherLines;
	private static String[] topWords;
	private static List<Integer> cipherInts = new ArrayList<>();
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {

		con = new Console("CTO");
		
		readTopWords();
		
		con.print("Enter Input File Name for Decryption: ");
		
		try {
			cipherTxt = new Scanner(new File(con.readWord()+".txt")).useDelimiter("\\Z").next();
			cipherLines	= cipherTxt.split("\n");
			cipherLen 	= cipherTxt.split("\n").length;

			for(int i=0; i<cipherLen; i++) {
				cipherInts.add(Hex16.convert(cipherLines[i]));
			}
			
			cipherKey = getKey(cipherLen);

			/**
			 * Decode string based on the cipherKey
			 */

			String tmpStr = "";
			int tmpInt;

			for(int i=0; i<cipherLen; i++) {
				tmpInt = Coder.decrypt(Hex16.convert(cipherKey), cipherInts.get(i));
				tmpStr += convertToText(tmpInt);
			}

			con.println("Key value ="+cipherKey);
			con.println(tmpStr);
			con.println("----------------------------------------");
			con.println("Calculating minimum number of blocks...");
			con.println("----------------------------------------");
			checkMinBlocks();
			
			con.println("-- Finished --");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Calculate key upto the number of blocks mentioned by mxLen
	 * @param mxLen
	 * @return
	 */
	
	public static String getKey(int mxLen) {
		int maxRank = -1;
		int tmpRank;
		String tmpKey="";
		for( int key=1;key<=Math.pow(2, 16);key++ ) {
			String tmpStr = "";
			int tmpInt;
			for(int i=0; i<mxLen; i++) {
				tmpInt = Coder.decrypt(key, cipherInts.get(i));
				tmpStr += convertToText(tmpInt);
			}
			tmpRank = checkIfEnglish(tmpStr);
			if( tmpRank>maxRank ) {
				maxRank 	= tmpRank;
				tmpKey 	= String.format("0x%04x", key);
			}
		}
		return tmpKey;
	}

	/**
	 * Check is String contains most common English words
	 * if yes then rank cipherText based in totally number of
	 * English words matching
	 * @param txtVal
	 * @return
	 */

	public static int checkIfEnglish(String txtVal){
		ArrayList<String> words = new ArrayList<String>();
		String[] tmpList = txtVal.split(" ");
		int rankValue =0;

		for(int i=0;i<tmpList.length;i++) {
			words.add(tmpList[i]);
		}
		for(int i=0;i<topWords.length;i++) {
			if( words.contains(topWords[i].trim()) ) {
				rankValue+=(topWords.length-i);
			}
		}
		return rankValue;
	}
	
	/**
	 * This method converts and integer value into an
	 * English word.
	 * @param intValue
	 * @return
	 */
	
	private static String convertToText(int intValue) {
		String txtVal = "";
		int	c0 = intValue/256;
		int	c1 = intValue%256;	
		txtVal += (char)c0;
		if( c1 != 0) txtVal+=(char)c1;
		return txtVal;
	}
	
	/**
	 * This method reads top-N English words and stores in
	 * an array
	 */

	private static void readTopWords() {
		String tmpWords;
		try {
			con.print("Enter English Words file name: ");
			tmpWords = new Scanner(new File(con.readWord()+".txt")).useDelimiter("\\Z").next();
			topWords = tmpWords.split("\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Find the minimum number of cipher text blocks required for decoding
	 * the complete message.
	 */

	public static void checkMinBlocks() {
		String curKey= ""; 
		for(int i=1; i<=cipherLen; i++) {
			curKey = getKey(i);
			con.println("Cipher text blocks used ="+i+" out of "+cipherLen);
			if( curKey.equals(cipherKey) ) {
				con.println("Minimum number of blocks needed ="+i);
				break;
			}
		}
	}
}
