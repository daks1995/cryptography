import FormatIO.Console;
import FormatIO.EofX;
import FormatIO.FileIn;

public class TMT2 {
	private static int chainLen, chainSize;
	private static Table tb = new Table();
	private static int key	= -1;
	private static Console	con;
	private static String cipherTxt, plainTxt;
	private static int cipherInt, plainInt;
	private static String file_in;
	
	/**
	 * Get the key value based on TMT table calculation
	 * 
	 */
	private static int findKey() {
		FileIn fin = new FileIn(file_in);

		// read blocks, encrypt and output blocks
		try
		{
			for (;;)
			{
				String[] s = fin.readLine().split(" ");
				tb.add( Integer.parseInt(s[0]), Integer.parseInt(s[1]) );
			}
		}
		catch (EofX x)
		{
		}
		
		fin.close();
		
		if( tb.find(cipherInt)!=-1 ) {
			key = checkIfInTable(tb.find(cipherInt));
		}else {
			int x2;
			int x1 = Coder.encrypt(cipherInt, plainInt);

			for(int i=0;i<chainSize;i++) {
				x2 = Coder.encrypt(x1, plainInt);
				if( tb.find(x2)!=-1 ) {
					key = checkIfInTable(tb.find(x2));
					if(key==-1) break;
				}
				x1 = Coder.decrypt(x2, plainInt);
				if( tb.find(x1)!=-1 ) {
					key = checkIfInTable(tb.find(x1));
					if(key==-1) break;
				}				
			}
		}
		return key;
	}
	
	public static void main(String[] args) {
		int N_ITER  = 10;
		chainLen	= (int) Math.pow(2,8);
		chainSize	= (int) Math.pow(2,8);

		// Create a Console for IO
		con = new Console("TMT2");

		// Get Plain text and convert it to Integer	
		con.print("Enter Plain Text:");
		plainTxt  = con.readWord();
		plainInt  = Hex16.convert(plainTxt);

		// Get Plain text and convert it to Integer	
		con.print("Enter Cipher Text:");
		cipherTxt  = con.readWord();
		cipherInt  = Hex16.convert(cipherTxt);
		
		con.print("Enter Output File Name: ");
		file_in = con.readWord()+".txt";

		for(int i=0;i<N_ITER;i++) {
			key = findKey();
			if(key!=-1) {
				con.println("Key found in iteration="+i);
				con.println("Key for decryption is = "+String.format("0x%04x", key));
				break;
			}else {
				con.println("Unable to find Key in iteration="+i+". Running TMT1 again.");
				TMT1.generateTMTTable(plainTxt,file_in);
			}
		}
		con.println("-- Finished --");
	}

	/**
	 * Get the key value based on random number corresponding to
	 * cipher value
	 * @param intVal
	 * @return
	 */
	public static int checkIfInTable(int intVal){
		int x2 = intVal;
		int x1 = Coder.encrypt(x2, plainInt);

		for( int i=0;i<chainLen;i++ ) {
			x2 = Coder.encrypt(x1, plainInt);
			if(x2==cipherInt) return x1;
			
			x1 = Coder.encrypt(x2, plainInt);
			if(x1==cipherInt) return x2;
		}
		return -1;
	}
}