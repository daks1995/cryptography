import FormatIO.Console;

public class KPT {
	/*
	 * This program calculates key for Known Plain Text Attack (KPT)
	 * based on Brute Force method. This program takes in
	 * know plain and cipher text and converts it to integer
	 * and runs Brute Force algorithm to check for each key if the de-crypted
	 * message matches plain text. I yes then that is the required key.
	 * 
	 */
	
	/*
	 *    Steps to decrypt original message from cipher-1.txt file.
	 * 1. Run KPT.java enter cipher and know plain text
	 * 2. Program shall output key note down this key.
	 * 3. Run DecryptAllBlocks.java use key noted down from previous
	 *    program to decrypt message to cipher-1_dc.txt
	 * 4. Run Block2Text.java to convert cipher-1_dc.txt (Block file)
	 *    to cipher-1_dc_t.txt (Actual Message file)
	 */

	public static void main(String[] args) {
		String	cipherTxt, plainTxt, cipherKey;
		int cipherInt, plainInt;

		// Create a Console for IO
		Console	con = new Console("KPT");

		// Get Cipher text and convert it to Integer
		con.print("Enter Cipher Text:");
		cipherTxt = con.readWord();
		cipherInt = Hex16.convert(cipherTxt);

		// Get Plain text and convert it to Integer	
		con.print("Enter Plain Text:");
		plainTxt  = con.readWord();
		plainInt  = Hex16.convert(plainTxt);

		cipherKey = "";

		// Run Brute Force Algorithm
		for( int key=1;key<=Math.pow(2,16); key++ ) {
			int	c = Coder.decrypt(key, cipherInt);
			if( c==plainInt ) {
				cipherKey = String.format("0x%04x", key);
				break;
			}
		}
		con.println("Key for decryption is = "+cipherKey);
		con.println("-- Finished --");
	}
}